/*
ETH Zürich
Gabriel Pregger
ITSC Helpdesk
Department of Biosystems Science and Engineering (D-BSSE)
BSS E 45
Klingelbergstrasse 48
4056 Basel, Switzerland
gabriel.pregger@bsse.ethz.ch
http://bsse.ethz.ch
*/
window.addEventListener('load', loadImageInterval);
let currentIndex = 1;
let nextIsVideo = false;
let lastFileList = null;  // Remember the file list to detect changes and trigger a reload if necessary
let timerIntervalId = null;

let slideTime;
let transitionTime;
let animation;
/*
Possible animations:
- verticalSlide
- horizontalSlide
- fade
- fadeZoom
- fadeZoomReverse
- pushUp
- pushLeft
- apple
- wipeToTopLeft (Chrome only)
- wipeToBottomRight (Chrome only)
*/

const configFile = 'images/configuration.txt';
const imageFileTypes = ['apng', 'png', 'jpg', 'jpeg', 'webp', 'gif'];
const videoFileTypes = ['mp4', 'webm'];
const errorImage = 'data:image/svg+xml;base64,PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4NCjwhLS0gVXBsb2FkZWQgdG86IFNWRyBSZXBvLCB3d3cuc3ZncmVwby5jb20sIFRyYW5zZm9ybWVkIGJ5OiBTVkcgUmVwbyBNaXhlciBUb29scyAtLT4KPHN2ZyBmaWxsPSIjZmZmZmZmIiB3aWR0aD0iODAwcHgiIGhlaWdodD0iODAwcHgiIHZpZXdCb3g9IjAgMCAyNTYgMjU2IiBpZD0iRmxhdCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBzdHJva2U9IiNmZmZmZmYiPgoNPGcgaWQ9IlNWR1JlcG9fYmdDYXJyaWVyIiBzdHJva2Utd2lkdGg9IjAiLz4KDTxnIGlkPSJTVkdSZXBvX3RyYWNlckNhcnJpZXIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIvPgoNPGcgaWQ9IlNWR1JlcG9faWNvbkNhcnJpZXIiPiA8cGF0aCBkPSJNMjAyLjgyODYxLDE5Ny4xNzE4OGEzLjk5OTkxLDMuOTk5OTEsMCwxLDEtNS42NTcyMiw1LjY1NjI0TDEyOCwxMzMuNjU3MjMsNTguODI4NjEsMjAyLjgyODEyYTMuOTk5OTEsMy45OTk5MSwwLDAsMS01LjY1NzIyLTUuNjU2MjRMMTIyLjM0MywxMjgsNTMuMTcxMzksNTguODI4MTJhMy45OTk5MSwzLjk5OTkxLDAsMCwxLDUuNjU3MjItNS42NTYyNEwxMjgsMTIyLjM0Mjc3bDY5LjE3MTM5LTY5LjE3MDg5YTMuOTk5OTEsMy45OTk5MSwwLDAsMSw1LjY1NzIyLDUuNjU2MjRMMTMzLjY1NywxMjhaIi8+IDwvZz4KDTwvc3ZnPg==';

/*
--------------------------------
 ENTRY POINT
--------------------------------
*/
async function loadImageInterval()
{
    await readConfiguration();
    
    // Fallback configuration
    if (!slideTime)
        slideTime = 20000;
    if (!transitionTime)
        transitionTime = 1000;
    if (!animation)
        animation = 'pushUp';

    document.querySelectorAll('video').forEach(videoElement => videoElement.addEventListener('ended', async function nextSlide() { transitionToNextSlide(); await readConfiguration(); }));
    loadFirstImage();
    setInitialPosition();
}

async function readConfiguration()
{
    var configuration;
    try {
        configuration = await fetch(configFile).then(response => response.json());
    } catch(error) {
        console.error(error);
        console.log('Config file not found or invalid. Using fallback config.');
        return;
    }
    
    if (configuration['transition-animation'])
        if (animation && animation !== configuration['transition-animation'])
            // Configuration changed, reload page
            location.reload();
        animation = configuration['transition-animation'];
    if (configuration['transition-duration'])
        if (transitionTime && transitionTime !== configuration['transition-duration'])
            // Configuration changed, reload page
            location.reload();
        transitionTime = configuration['transition-duration'];
    if (configuration['slide-time'])
        if (slideTime && slideTime !== configuration['slide-time'])
            // Configuration changed, reload page
            location.reload();
        slideTime = configuration['slide-time'];
}

async function loadFirstImage()
{
    const imageFiles = await getImageFiles();

    const currentSlideMedia = imageFiles[0];

    const isVideo = isVideoFile(currentSlideMedia);

    if (isVideo)
    {
        var currentImageNode = document.querySelector('#current > video');
    } else {
        var currentImageNode = document.querySelector('#current > img');
        setTimeout(async () => { transitionToNextSlide(); await readConfiguration(); }, slideTime);
    }

    currentImageNode.src = currentSlideMedia || errorImage;
    if (isVideo)
    {
        currentImageNode.play();
    }

    currentImageNode.style.display = 'block';

    loadNextImage();
}

function setInitialPosition()
{
    const nextSlide = document.getElementById('next');
    const currentSlide = document.getElementById('current');

    setUpSlideAnimation(currentSlide, nextSlide);
}

async function loadNextImage()
{
    const imageFiles = await getImageFiles();

    if (currentIndex >= imageFiles.length)
    {
        currentIndex = 0;
    }
    const nextSlideMedia = imageFiles[currentIndex];

    if (isVideoFile(nextSlideMedia))
    {
        var nextImageNode = document.querySelector('#next > video');
        document.querySelector('#next > img').src = '';
        document.querySelector('#next > img').style.display = 'none';
        nextIsVideo = true;
    } else {
        var nextImageNode = document.querySelector('#next > img');
        document.querySelector('#next > video').src = '';
        document.querySelector('#next > video').style.display = 'none';
        nextIsVideo = false;
    }

    nextImageNode.src = nextSlideMedia || errorImage;
    nextImageNode.style.display = 'block';

    currentIndex += 1;
}

function isVideoFile(filePath)
{
    return filePath && videoFileTypes.includes(filePath.split('.').slice(-1)[0].toLowerCase());
}

function setUpNewNextSlide()
{
    let currentSlide = document.getElementById('current');
    let nextSlide = document.getElementById('next');

    currentSlide.style.transition = '';

    [...currentSlide.children].forEach(child => {child.src = '';});
    nextSlide.id = 'current';
    currentSlide.id = 'next';

    currentSlide = document.getElementById('current');
    nextSlide = document.getElementById('next');

    setUpSlideAnimation(currentSlide, nextSlide);

    loadNextImage();
}

async function getImageFiles()
{
    const fileListingHtml = await fetch('images').then(response => response.text());
    const fileListingDoc = new DOMParser().parseFromString(fileListingHtml, 'text/html');
    const fileListLinks = fileListingDoc.getElementsByTagName('a');
    const imageFiles = Array.from(fileListLinks).map((file) => {
        const fileName = file.attributes.href.nodeValue;
        const fileType = fileName.split('.').slice(-1)[0].toLowerCase();
        if (imageFileTypes.includes(fileType) || videoFileTypes.includes(fileType)){
            return file.baseURI + 'images/' + file.attributes.href.nodeValue;
        }
    }).sort((a, b) => a.localeCompare(b, 'de-CH-u-kn-true')).filter((item) => item !== undefined);

    if (lastFileList !== null && !compareFileList(imageFiles))
    {
        // File list changed, reload page so new show starts at beginning
        location.reload();
    }

    lastFileList = imageFiles;
    return imageFiles;
}

function compareFileList(newFileList)
{
    if (newFileList.length !== lastFileList.length)
    {
        return false;
    }

    for (let i=0; i<newFileList.length; i++)
    {
        if (newFileList[i] !== lastFileList[i])
        {
            return false;
        }
    }
    return true;
}

/*
--------------------------------
 Animation Specific code from here
--------------------------------
*/
function setUpSlideAnimation(currentSlide, nextSlide)
{
    switch (animation)
    {
        case 'verticalSlide':
            nextSlide.style.position = 'absolute';
            nextSlide.style.bottom = '100vh';
            nextSlide.style.zIndex = '100';
            break;
        case 'horizontalSlide':
            nextSlide.style.position = 'absolute';
            nextSlide.style.left = '100vw';
            nextSlide.style.zIndex = '100';
            break;
        case 'fade':
        case 'fadeZoom':
            nextSlide.style.position = 'absolute';
            nextSlide.style.left = '0';
            nextSlide.style.top = '0';
            nextSlide.style.opacity = '0';
            nextSlide.style.transform = 'scale(1.0)';
            break;
        case 'fadeZoomReverse':
            nextSlide.style.position = 'absolute';
            nextSlide.style.left = '0';
            nextSlide.style.top = '0';
            nextSlide.style.opacity = '0';
            nextSlide.style.transform = 'scale(1.1)';
            break;
        default: case 'pushUp':
            nextSlide.style.position = 'absolute';
            nextSlide.style.top = '100vh';
            break;
        case 'pushLeft':
            nextSlide.style.position = 'absolute';
            nextSlide.style.left = '100vw';
            break;
        case 'apple':
            nextSlide.style.position = 'absolute';
            nextSlide.style.left = '50vw';
            nextSlide.style.top = '0';
            nextSlide.style.opacity = '0';
            nextSlide.style.transform = 'perspective(800px) rotateY(-45deg)';
            break;
        case 'wipeToTopLeft':
            nextSlide.style.position = 'absolute';
            nextSlide.style.left = '0';
            nextSlide.style.top = '0';
            nextSlide.style.setProperty('--wipe-position', '100%');
            nextSlide.style.setProperty('--gradient-length', '0.1%');
            nextSlide.style.maskImage = 'linear-gradient(to bottom right, black var(--wipe-position), transparent calc(var(--wipe-position) + var(--gradient-length)), transparent)';
            currentSlide.style.maskImage = 'linear-gradient(to bottom right, black var(--wipe-position), transparent calc(var(--wipe-position) + var(--gradient-length)), transparent)';
            break;
        case 'wipeToBottomRight':
            nextSlide.style.position = 'absolute';
            nextSlide.style.left = '0';
            nextSlide.style.top = '0';
            nextSlide.style.setProperty('--wipe-position', '100%');
            nextSlide.style.setProperty('--gradient-length', '0.1%');
            nextSlide.style.maskImage = 'linear-gradient(to top left, black var(--wipe-position), transparent calc(var(--wipe-position) + var(--gradient-length)), transparent)';
            currentSlide.style.maskImage = 'linear-gradient(to top left, black var(--wipe-position), transparent calc(var(--wipe-position) + var(--gradient-length)), transparent)';
            break;
    }
}

function transitionToNextSlide()
{
    const nextSlideContainer = document.getElementById('next');
    const currentSlideContainer = document.getElementById('current');

    nextSlideContainer.style.transition = 'ease-in-out ' + transitionTime + 'ms';
    currentSlideContainer.style.transition = 'ease-in-out ' + transitionTime + 'ms';

    switch (animation)
    {
        case 'verticalSlide':
            currentSlideContainer.style.zIndex = '0';
            nextSlideContainer.style.bottom = '0';
            break;
        case 'horizontalSlide':
            currentSlideContainer.style.zIndex = '0';
            nextSlideContainer.style.left = '0';
            break;
        case 'fade':
            currentSlideContainer.style.zIndex = '0';
            nextSlideContainer.style.opacity = '100';
            currentSlideContainer.style.opacity = '0';
            break;
        case 'fadeZoom':
            currentSlideContainer.style.zIndex = '0';
            nextSlideContainer.style.opacity = '100';
            currentSlideContainer.style.opacity = '0';
            currentSlideContainer.style.transform = 'scale(1.1)';
            break;
        case 'fadeZoomReverse':
            currentSlideContainer.style.zIndex = '0';
            nextSlideContainer.style.opacity = '100';
            nextSlideContainer.style.transform = 'scale(1)';
            currentSlideContainer.style.opacity = '0';
            break;
        default: case 'pushUp':
        currentSlideContainer.style.zIndex = '0';
            currentSlideContainer.style.top = '-100vh';
            nextSlideContainer.style.top = '0';
            break;
        case 'pushLeft':
            currentSlideContainer.style.zIndex = '0';
            currentSlideContainer.style.left = '-100vw';
            nextSlideContainer.style.left = '0';
            break;
        case 'apple':
            currentSlideContainer.style.zIndex = '0';
            nextSlideContainer.style.left = '0';
            nextSlideContainer.style.transform = 'perspective(800px) rotateY(0)';
            nextSlideContainer.style.opacity = '100';
            currentSlideContainer.style.left = '-50vw';
            currentSlideContainer.style.transform = 'perspective(800px) rotateY(45deg)';
            currentSlideContainer.style.opacity = '0';
            break;
        case 'wipeToTopLeft':
        case 'wipeToBottomRight':
            currentSlideContainer.style.transition = '--wipe-position ' + transitionTime + 'ms ease';
            currentSlideContainer.style.setProperty('--wipe-position', '0%');
            break;
    }

    setTimeout(setUpNewNextSlide, transitionTime);

    if (nextIsVideo)
    {
        nextSlideContainer.getElementsByTagName('video')[0].play();
    } else {
        setTimeout(async () => { transitionToNextSlide(); await readConfiguration(); }, slideTime);
    }
}