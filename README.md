# BSSE-JSlideshow

Simple JavaScript and CSS based Slideshow generator with several built in transitions.
  
**Documentation on the BSSE-Wiki:**  
<https://wiki-bsse.ethz.ch/display/~gpregger/BSSE-JSlideshow>